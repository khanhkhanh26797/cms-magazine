const firebase = require('firebase-admin');
const functions = require('firebase-functions');
const cors = require('cors')({ origin: true });
firebase.initializeApp();

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
exports.notificationUser = functions.https.onRequest((req, res) => {
    let { fcmId, title, body } = req.body
    return cors(req, res, () => {
      if (req.method !== 'POST') {
        return res.status(401).json({
          message: 'Not allowed'
        });
      };
      firebase.messaging().sendToDevice([fcmId], {
        notification: { title, body, }
      }).then(data => {
        console.log('data', data)
        res.send(data)
      }).catch(error => {
        res.send(error)
      })
  
    });
  });
